--
-- Table structure for table `infos`
--

CREATE TABLE `infos` (
  `id` int(11) NOT NULL,
  `amount` varchar(100) DEFAULT NULL,
  `tran_id` varchar(100) DEFAULT NULL,
  `desc` varchar(100) DEFAULT NULL,
  `custom` varchar(100) DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `cus_name` varchar(100) DEFAULT NULL,
  `cus_email` varchar(100) DEFAULT NULL,
  `cus_add1` varchar(100) DEFAULT NULL,
  `cus_add2` varchar(100) DEFAULT NULL,
  `cus_city` varchar(100) DEFAULT NULL,
  `cus_state` varchar(100) DEFAULT NULL,
  `cus_postcode` varchar(100) DEFAULT NULL,
  `cus_country` varchar(100) DEFAULT NULL,
  `cus_phone` varchar(100) DEFAULT NULL,
  `cus_fax` varchar(100) DEFAULT NULL,
  `ship_name` varchar(100) DEFAULT NULL,
  `ship_add1` varchar(100) DEFAULT NULL,
  `ship_add2` varchar(100) DEFAULT NULL,
  `ship_city` varchar(100) DEFAULT NULL,
  `ship_state` varchar(100) DEFAULT NULL,
  `ship_postcode` varchar(100) DEFAULT NULL,
  `ship_country` varchar(100) DEFAULT NULL,
  `Option A` varchar(100) DEFAULT NULL,
  `Option B` varchar(100) DEFAULT NULL,
  `Option C` varchar(100) DEFAULT NULL,
  `Option D` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT 'due',
  `payment_processor` varchar(25) DEFAULT NULL,
  `cardnumber` varchar(25) DEFAULT NULL,
  `cardname` varchar(50) DEFAULT NULL,
  `bank_trxid` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Indexes for table `infos`
--
ALTER TABLE `infos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `infos`
--
ALTER TABLE `infos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;
